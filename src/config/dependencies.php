<?php
return [
    UserController::class=>[
        \App\Database\DBConnector::class,
        \App\Dao\UserUserDao::class,
        \App\Views\Renderer\ViewRenderer::class
    ]
];