<?php
require_once __DIR__.'/../../vendor/autoload.php';
$settings = require_once __DIR__.'/config.php';
$db = require_once __DIR__.'/database.php';
$auth =  require_once __DIR__.'/auth.php';
$dependencies = require_once __DIR__.'/dependencies.php';
$validators = require_once __DIR__.'/validations.php';

$generalConfig = [
    'settings'=>$settings,
    'db'=>$db,
    'auth'=>$auth,
    'dependencies'=>$dependencies,
    'validators'=>$validators
];

$app = new \App\Kernel\Application($generalConfig);

