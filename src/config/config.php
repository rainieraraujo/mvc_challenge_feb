<?php
return [
    'APP_PROJECT_URL'=> '/check24challenge/public/',
    'APP_ROOT'=> __DIR__.'/../../',
    'CONTROLLER_NAMESPACE'=> 'App\\Controllers\\',
    'CONTROLLER_DEFAULT'=> 'Home',
    'CONTROLLER_DEFAULT_METHOD'=> 'index',
    'CONTROLLER_SUFIX'=>'Controller'
];