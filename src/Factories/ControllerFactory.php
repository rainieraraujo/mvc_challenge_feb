<?php

namespace App\Controllers;
use App\Kernel\IRequest;
use App\Controllers\User\UserController;

class ControllerFactory
{
    public static function __invoke(array $config, IRequest $request):IController
    {
        $controller = null;
        switch ($request->getRequestController()){
            case UserController::class:
                $conn = new \App\Database\DBConnector($config['db']);
                $dao = new \App\Dao\UserUserDao($conn->connect());
                $controller = new UserController($request,$dao,$config['auth']);
                break;
            default:
                $controller =  new $request->getRequestController();
                break;
        }
        return$controller;
    }
}