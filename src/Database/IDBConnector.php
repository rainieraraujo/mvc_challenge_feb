<?php


namespace App\Database;


interface IDBConnector
{
    public function connect();
}