<?php

namespace App\Database;

class DBConnector implements IDBConnector
{
    private $config;
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function connect(){
        try {
            $connString = $this->config['DB_DRIVER'].":host=".$this->config['DB_HOST'].";dbname=".$this->config['DB_NAME'];
            return new \PDO($connString, $this->config['DB_USER'], $this->config['DB_PASS']);
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }
}