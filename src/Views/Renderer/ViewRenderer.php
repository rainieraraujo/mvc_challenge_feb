<?php


namespace App\Views\Renderer;


class ViewRenderer
{
    private $viewDir = '/src/Views';
    private $layout = 'index.php';
    private $reservedVariables = ['application_name', 'body'];

    public function getView($controller,$view, array $data = [])
    {
        $variables = $this->validateReservedVariables($data);

        $parts = str_replace ('Controller', '',$controller);

        $viewPath = $this->viewDir.'/'.$parts.'/'.$view.'.php';
        if (file_exists($viewPath)) {
            $baseView = file_get_contents($this->viewPath.'/'.$this->layout);
            $body = file_get_contents($viewPath);
            $view = str_replace('{{ body }}', $body, $baseView);

            foreach ($variables as $key => $value) {
                $view = str_replace('{{ '.$key.' }}', $value, $view);
            }
            return $view;
        }
        http_response_code(404);
        throw new Exception(sprintf('View cannot be found: [%s]', $viewPath), 404);
    }

    private function validateReservedVariables(array $variables = [])
    {
        foreach ($variables as $name => $value) {
            if (in_array($name, $this->reservedVariables)) {
                http_response_code(404);
                throw new Exception('Can not process provided variables');
            }
        }

        return $variables;
    }

    private function getDirectory($controller)
    {
        $parts = explode('\\', $controller);

        return end($parts);
    }

    private function getFile($controller)
    {
        return str_replace(APP_CONTROLLER_METHOD_SUFFIX, null, $controller);
    }
}