<?php

namespace App\Dao;

use App\Database\IDBConnector;
use App\Models\IUserModel;
use App\Models\User;

class UserUserDao implements IUserDao,IDao
{
    private $conn;

    /**
     * UserUserDao constructor.
     * @param IDBConnector $connection
     */
    public function __construct(IDBConnector $connection)
    {
        $this->conn = $connection;
    }

    /**
     * @return array
     */
    public function getAll():array{
        $userQuery = $this->conn->prepare("SELECT * FROM users");
        $userQuery->setFetchMode(\PDO::FETCH_ASSOC);
        $userQuery->execute();
        $users = [];
        while ($user = $userQuery->fetch()){
            $model = new User();
            $model->exchangeArray($user);
            $users[]= $model;
        }
        return $users;
    }

    /**
     * @param integer $id
     * @return User
     */
    public function getById(integer $id):IUserModel{
        $userQuery = $this->conn->prepare("SELECT * FROM users WHERE ID=".$id);
        $userQuery->setFetchMode(\PDO::FETCH_ASSOC);
        $userQuery->execute();
        while ($user = $userQuery->fetch()){
            $result = new User();
            $result->exchangeArray($user);
        }
        return $result;
    }

    /**
     * @param string $username
     * @return User
     */
    public function getByUsername(string $username):IUserModel{
        $userQuery = $this->conn->prepare("SELECT * FROM users WHERE USERNAME= ?");
        $userQuery->setFetchMode(\PDO::FETCH_ASSOC);
        $userQuery->execute();
        $result = null;
        while ($user = $userQuery->fetch()){
            $result = new User();
            $result->exchangeArray($user);
        }

        return $result;
    }
}