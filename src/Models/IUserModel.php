<?php


namespace App\Models;


interface IUserModel
{
    /**
     * @param array $data
     */
    public function exchangeArray(array $data);

    /**
     * @return array
     */
    public function getArrayCopy():array;
}