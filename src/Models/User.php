<?php
namespace App\Models;

class User implements IUserModel
{
    private $id;
    private $username;
    private $password;
    private $email;

    /**
     * @return integer
     */
    public function getId():integer
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername():string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword():string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getEmail():string
    {
        return $this->email;
    }

    /**
     * @param array $data
     */
    public function exchangeArray(array $data){
        $this->id = !empty($data['id'])?(int)$data['id']:null;
        $this->username = !empty($data['username'])?$data['username']:null;
        $this->email = !empty($data['email'])?$data['email']:null;
        $this->password = !empty($data['password'])?$data['password']:null;
    }

    /**
     * @return array
     */
    public function getArrayCopy():array{
        return get_object_vars($this);
    }
}