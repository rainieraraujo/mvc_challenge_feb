<?php
namespace App\Kernel\DependencyContainer;

interface IContainer
{
    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key);

    /**
     * @param string $key
     * @param $callable
     * @return mixed
     */
    public function set(string $key, $callable);

    /**
     * @param array $dependencies
     * @return mixed
     */
    public function register(array $dependencies);
}