<?php
namespace App\Kernel\DependencyContainer;

class Container implements IContainer
{
    /**
     * @var array dependencies
     */
    private $dependencies;

    public function __construct(array $config=[])
    {
        foreach ($config as $key => $value){
            $this->dependencies[$key] = $value;
        }
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        return $this->dependencies[$key];
    }

    /**
     * @param string $key
     * @param $callable
     */
    public function set(string $key, $callable)
    {
        $this->dependencies[$key] = $callable;
    }

    /**
     * @param array $dependencies
     */
    public function register(array $dependencies)
    {
        foreach ($dependencies as $key => $value){
            $this->dependencies[$key] = $value;
        }
    }
}