<?php


namespace App\Kernel;


use App\Controllers\IController;

interface IRequest
{
    public function getRequestController():string;
    public function getRequestAction(IController $controller):string;
    public function getRequestPost():array;
    public function getRequestGet():array;
}