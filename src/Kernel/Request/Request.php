<?php
namespace App\Kernel;

use App\Controllers\IController;
use App\Validators\IRequestValidator;
use Exception;

class Request implements IRequest
{
    private $server;
    private $post;
    private $get;
    private $method;
    /**
     * @var array config
     */
    private $config;
    /**
     * @var IRequestValidator validator
     */
    private $validator;

    public function __construct(
        array $server,
        array $post = [],
        array $get = [],
        array $config = []
    ) {
        $this->serverURI = $server['REQUEST_URI'];
        $this->post = $post;
        $this->get = $get;
        $this->config = $config;
        $this->method = $server['REQUEST_METHOD'];
        unset($server,$post,$get,$config);
    }

    /**
     * @return array
     */
    public function getRequestPost():array{
        return $this->post;
    }

    /**
     * @return array
     */
    public function getRequestGet():array{
        return $this->get;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getRequestController():string{
        $urlComponents = $this->getUrlComponents();

        if (!isset($urlComponents['controller'])) {
            return $this->config['CONTROLLER_NAMESPACE'].$this->config['CONTROLLER_DEFAULT'];
        }
        $controllerClass = $this->buildController($urlComponents['controller']);
        if (class_exists($controllerClass)) {
            return $controllerClass;
        }
        throw new Exception(sprintf('Page not found'), 404);
    }

    /**
     * @param IController $controller
     * @return string
     * @throws Exception
     */
    public function getRequestAction(IController $controller):string
    {
        $urlComponents = $this->getUrlComponents();
        if (empty($urlComponents['action'])) {
            return $this->config['CONTROLLER_DEFAULT_METHOD'];
        }

        if (!preg_match('/^[a-z\-]+$/', $urlComponents['action'])) {
            throw new Exception(sprintf('Invalid action: [%s]', $urlComponents['action']), 400);
        }

        $action = $this->buildAction($urlComponents['action']);
        if (method_exists($controller, $action)) {
            return $action;
        }
        throw new Exception(sprintf('Action not found: [%s:%s]', $controller, $action), 404);
    }

    /**
     * @return array
     */
    private function getUrlComponents()
    {
        $requestPath = str_replace($this->config['APP_PROJECT_URL'], null, $this->serverURI);
        $urlComponents = array_values(explode('/', $requestPath));
       return [
            'controller' => $urlComponents[0],
            'action' =>isset($urlComponents[1])?$urlComponents[1]:''
        ];
    }

    /**
     * @param $requestedAction
     * @return mixed|string|null
     */
    private function buildAction($requestedAction):string{
        $action = null;
        $parts = explode('-', $requestedAction);
        foreach ($parts as $part) {
            if (!$action) {
                $action = $part;
            } else {
                $action .= ucfirst($part);
            }
        }
        return $action;
    }

    /**
     * @param $controller
     * @return string
     */
    private function buildController($controller):string{
        return $this->config['CONTROLLER_NAMESPACE'].ucfirst($controller).$this->config['CONTROLLER_SUFIX'];
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }


    /**
     * @param IRequestValidator $validator
     */
    public function setValidator(IRequestValidator $validator){
        $this->validator;
    }
}
