<?php
namespace App\Kernel;

use App\Kernel\DependencyContainer\Container;
use App\Kernel\DependencyContainer\IContainer;

class Application
{
    /**
     * @var IContainer Container
     */
    private $container;
    /**
     * @var IRequest Request
     */
    private $request;

    public function __construct(array $config)
    {
        $this->container = new Container($config);
        $this->request = new Request($_SERVER['REQUEST_URI'], $_POST, $_GET,$config);
        unset($config);
    }
    /**
     * @return Request
     */
    public function getContainer(){
        return $this->container;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    public function process(){
        $controller = \App\Controllers\ControllerFactory::getController($this->getContainer(),$this->request);
        $method = $this->request->getRequestAction($controller->getControllerName());
        if(is_callable(array($controller,$method))){
            return $controller->$method();
        }
        else{
            return json_encode('Unhandled Exception');
        }
    }





}