<?php
namespace App\Controllers\User;
use Firebase\JWT\JWT;
use App\Controllers\AbstractController;

class UserController extends AbstractController implements IUserController
{
    const INVALID_CREDENTIALS = 'Invalid User Credentials';

    public function getControllerName(){
        return __CLASS__;
    }

    public function login(){
        if(!empty($this->request->getRequestPost()['username']) AND !empty($this->request->getRequestPost()['password'])) {
            $user = $this->dao->getByUsername($this->request->getRequestPost()['username']);
            if(!empty($user)){
                $response = $this->validateCredentials($user->getPassword())?$this->buildToken($user):self::INVALID_CREDENTIALS;
            }else{
                return json_encode(self::INVALID_CREDENTIALS);
            }
        }
        else{
            return json_encode($response = [
                'Invalid User Credentials'
            ]);
        }
        return json_encode($response);
    }

    private function buildToken($user){
        $token = array(
            "iss" => $this->config['ISS'],
            "aud" => $this->config['AUD'],
            "iat" => time(),
            "exp" => time()+3600,
            "data" => array(
                "id" => $user->getId(),
                "username" => $user->getUsername(),
                "email" =>  $user->getEmail(),
            ));
        return JWT::encode($token, $this->config['JWT_SECRET']);
    }

    private function validateCredentials($password){
        if($password === $this->request->getRequestPost()['password']){
            $response = [
                'token'=> $token
            ];
            http_response_code(200);
            return json_encode($response);
        }
        else {
            http_response_code(401);
            $response = [
                'Unauthorized'
            ];
        }
    }
}