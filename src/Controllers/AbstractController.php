<?php
namespace App\Controllers;

use App\Dao\IDao;
use App\Kernel\IRequest;

class AbstractController implements IController
{
    protected $viewRenderer;
    protected $request;
    protected $dao;
    protected $config;

    public function __construct(IRequest $request, IDao $dao, array$config){
        $this->request = $request;
        $this->dao = $dao;
        $this->config = $config;
    }
    public function renderView($controllerName, string $view, array $variables=[]){
        return $this->viewRenderer->getView($controllerName,$view,$variables);
    }

    public function healthCheck()
    {
        http_response_code(200);
        return 'ack';
    }

}