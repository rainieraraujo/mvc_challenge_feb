<?php
namespace App\Validators;

use App\Exceptions\RequiredParameterException;
use App\Kernel\IRequest;

class LoginRequestValidator implements IRequestValidator
{
    const RULES = [
        'method'=>'post',
        'parameters'=>[
            'username'=>[
                'required'=>true,
                'type'=>'string',
                'allowEmpty'=>false
            ],
            'password'=>[
                'required'=>true,
                'type'=>'string',
                'allowEmpty'=>false
            ]
        ]
    ];

    /**
     * @param IRequest $request
     * @return bool
     * @throws RequiredParameterException
     */
    public function validate(IRequest $request):boolean
    {
        $this->validateMethod($request['method']);
        $this->validateParams($request->getRequestPost());
        return true;
    }

    /**
     * @param string $method
     * @return bool
     */
    private function validateMethod(string $method){
        return self::RULES === strtolower($method);
    }

    /**
     * @param array $params
     * @throws RequiredParameterException
     */
    private function validateParams(array $params){
        foreach (self::RULES['parameters'] as $key => $value){
            if ($value['required'])
                $this->validateRequired($key,$params);
            $this->validateType($value['type'],$params['$key']);
        }
    }

    /**
     * @param string $key
     * @param array $params
     * @return bool
     * @throws RequiredParameterException
     */
    private function validateRequired(string $key,array $params):bool{
        if(empty($params[$key])){
            throw new RequiredParameterException("Parameter $key is required");
        }
        else {
            return true;
        }
    }

    /**
     * @param string $rule
     * @param $params
     * @return bool
     * @throws RequiredParameterException
     */
    private function validateType(string $rule, $params):bool{
        if (!settype($params,$rule)){
            throw new RequiredParameterException("Parameter is expected to be $rule type");
        }else{
            return true;
        }

    }
}