<?php


namespace App\Validators;


use App\Kernel\IRequest;

interface IRequestValidator
{
    public function validate(IRequest $request):bool;
}