<?php
require_once __DIR__.'/../src/config/bootstrap.php';

try {
    echo $app->process();
} catch (Exception $e) {
    echo sprintf(
        '<h3>%s</h3>',
        $e->getMessage()
    );
}